@extends("layouts.main")

@section('tittle')
    <title>Thông tin chi tiết</title>
@endsection

@section('link')
    @include('components.link')
@endsection

@section('menu')
    @include('components.tenant_menu')
@endsection

@section('content')
<section class="ftco-section ftco-property-details goto-here">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="property-details">
                    <div id="carouselExampleControls" class="carousel slide img rounded" data-ride="carousel">
                        <div class="carousel-inner">            
                                <div class="carousel-item active">
                                    <img class="d-block w-100" src="{{asset('room_images/'.$detailRoom['image'][0]['url'])}}" alt="First slide">
                                </div>  
                            @foreach ($detailRoom['image'] as $item)               
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="{{asset('room_images/'.$item['url'])}}" alt="First slide">
                                </div> 
                            @endforeach   
                        </div>
                       
                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                          <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                          <span class="carousel-control-next-icon" aria-hidden="true"></span>
                          <span class="sr-only">Next</span>
                        </a>
                      </div>
                    <div class="text">
                        <h2>{{$detailRoom['tittle']}}</h2>
                        <span class="subheading">2854 Meadow View Drive, Hartford, USA</span>
                    </div>
                </div>
            </div>
        </div>
        <div style="margin-top: 30px" class="row">
            <div class="col-md-12 pills">
                      <div class="bd-example bd-example-tabs">
                          <div class="d-flex">
                            <ul class="nav nav-pills mb-2" id="pills-tab" role="tablist">

                              <li class="nav-item">
                                <a class="nav-link active" id="pills-description-tab" data-toggle="pill" href="#pills-description" role="tab" aria-controls="pills-description" aria-expanded="true">Thông tin phòng</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" id="pills-manufacturer-tab" data-toggle="pill" href="#pills-manufacturer" role="tab" aria-controls="pills-manufacturer" aria-expanded="true">Mô tả</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" id="pills-manufacturer-tab" onclick="return confirm('Bạn chắc chắn muốn đặt phòng này?')"  href="{{route('booking.room', ['room_id' => $detailRoom['id'], 'id_host' => $detailRoom['user_id']])}}" role="tab" aria-controls="pills-manufacturer" aria-expanded="true">Đặt phòng</a>
                              </li>
                            </ul>
                          </div>
                        <div class="tab-content" id="pills-tabContent">
                          <div class="tab-pane fade show active" id="pills-description" role="tabpanel" aria-labelledby="pills-description-tab">
                              <div class="row">
                                  <div class="col-md-4">
                                      <ul class="features">
                                          <li class="check"><span class="ion-ios-checkmark-circle"></span>Diện tích: {{$detailRoom['area']}}</li>
                                          <li class="check"><span class="ion-ios-checkmark-circle"></span>Giá phòng: {{$detailRoom['cost']}}</li>
                                          <li class="check"><span class="ion-ios-checkmark-circle"></span>Họ tên chủ phòng: {{$detailRoom['user']['name']}}</li>
                                          <li class="check"><span class="ion-ios-checkmark-circle"></span>Số điện thoại chủ phòng: {{$detailRoom['user']['phone_number']}}</li>
                                          <li class="check"><span class="ion-ios-checkmark-circle"></span>Email chủ phòng: {{$detailRoom['user']['email']}}</li>
                                      </ul>
                                  </div>
                              </div>
                          </div>

                          <div class="tab-pane fade" id="pills-manufacturer" role="tabpanel" aria-labelledby="pills-manufacturer-tab">
                            <p>{{$detailRoom['content']}}</p>
                          </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
@endsection

@section('js')
    @include('components.js')
@endsection