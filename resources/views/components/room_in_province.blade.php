<section class="ftco-section goto-here">
    <div class="container">
        <div class="row justify-content-center">
      <div class="col-md-12 heading-section text-center ftco-animate mb-5">
          <span class="subheading">Find Properties</span>
        <h2 class="mb-2">Find Properties In Your City</h2>
      </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="listing-wrap img rounded d-flex align-items-end" style="background-image: url(images/listing-1.jpg);">
                <div class="location">
                    <span class="rounded">New York, USA</span>
                </div>
                <div class="text">
                    <h3><a href="#">100 Property Listing</a></h3>
                    <a href="#" class="btn-link">See All Listing <span class="ion-ios-arrow-round-forward"></span></a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="listing-wrap img rounded d-flex align-items-end" style="background-image: url(images/listing-2.jpg);">
                <div class="location">
                    <span class="rounded">New York, USA</span>
                </div>
                <div class="text">
                    <h3><a href="#">100 Property Listing</a></h3>
                    <a href="#" class="btn-link">See All Listing <span class="ion-ios-arrow-round-forward"></span></a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="listing-wrap img rounded d-flex align-items-end" style="background-image: url(images/listing-3.jpg);">
                <div class="location">
                    <span class="rounded">New York, USA</span>
                </div>
                <div class="text">
                    <h3><a href="#">100 Property Listing</a></h3>
                    <a href="#" class="btn-link">See All Listing <span class="ion-ios-arrow-round-forward"></span></a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="listing-wrap img rounded d-flex align-items-end" style="background-image: url(images/listing-4.jpg);">
                <div class="location">
                    <span class="rounded">New York, USA</span>
                </div>
                <div class="text">
                    <h3><a href="#">100 Property Listing</a></h3>
                    <a href="#" class="btn-link">See All Listing <span class="ion-ios-arrow-round-forward"></span></a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="listing-wrap img rounded d-flex align-items-end" style="background-image: url(images/listing-5.jpg);">
                <div class="location">
                    <span class="rounded">New York, USA</span>
                </div>
                <div class="text">
                    <h3><a href="#">100 Property Listing</a></h3>
                    <a href="#" class="btn-link">See All Listing <span class="ion-ios-arrow-round-forward"></span></a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="listing-wrap img rounded d-flex align-items-end" style="background-image: url(images/listing-6.jpg);">
                <div class="location">
                    <span class="rounded">New York, USA</span>
                </div>
                <div class="text">
                    <h3><a href="#">100 Property Listing</a></h3>
                    <a href="#" class="btn-link">See All Listing <span class="ion-ios-arrow-round-forward"></span></a>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>