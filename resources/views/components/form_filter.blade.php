<section class="ftco-section ftco-no-pb goto-here">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="search-wrap-1 ftco-animate">
            <form action="{{route('filter.room')}}" method="post" class="search-property-1">
            @csrf
              <div class="row">
                  <div class="col-lg align-items-end">
                    <span>Tỉnh:</span><br>
                    <select class="form-select province_city" name="province_id" id="select" aria-label="Default select example">
                        <option value="">--Tỉnh/Thành phố--</option>
                        @foreach ($province as $item)
                            <option value="{{$item['code']}}">{{$item['name']}}</option>
                        @endforeach
                    </select>       
                  </div>
                  <div class="col-lg align-items-end">
                    <span>Huyện:</span><br>
                    <select class="form-select town" name="town_id" id="" aria-label="Default select example">
                        <option value="">--Huyện/Thị xã--</option>
                    </select>      
                  </div>
                  <div class="col-lg align-items-end">
                    <span>Giá:</span><br>
                    <select class="form-select town" name="cost" id="" aria-label="Default select example">
                        <option value="">--Giá--</option>
                        <option value="1">1 - 2 triệu</option>
                        <option value="2">2 - 3 triệu</option>
                        <option value="3">3 - 4 triệu</option>
                        <option value="4">> 4 triệu</option>
                    </select>          
                  </div>
                  <div class="col-lg align-items-end">
                    <span>Diện tích:</span><br>
                    <select class="form-select town" name="area" id="" aria-label="Default select example">
                        <option value="">--Diện tích--</option>
                        <option value="10">10 - 15 m2</option>
                        <option value="15">15 - 20 m2</option>
                        <option value="20">> 20 m2</option>
                    </select>              
                  </div>
                  <div class="col-lg align-self-end">
                      <div class="form-group">
                          <div class="form-field">
                            <input type="submit" value="Search Property" class="form-control btn btn-primary">
                          </div>
                      </div>
                  </div>
              </div>
              </form>
            </div>
          </div>
      </div>
  </div>
</section>