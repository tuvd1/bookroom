<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar ftco-navbar-light" id="ftco-navbar" style="background: transparent; position: fixed; left:0; right:0; top: 0px !important">
    <div class="container">
    <a class="navbar-brand" href="index.html">Booking Trọ</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="oi oi-menu"></span> Menu
    </button>

    <div class="collapse navbar-collapse" id="ftco-nav">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item {{url()->current() == 'http://bookroom.test/host' ? 'active' : ''}}"><a href="{{route('host.index')}}" class="nav-link">Trang chủ</a></li>
            <li class="nav-item {{url()->current() == 'http://bookroom.test/host/listBooking' ? 'active' : ''}}"><a href="{{route('list.booking.room')}}" class="nav-link">Danh sách phòng đang được đặt</a></li>
            <li class="nav-item {{url()->current() == 'http://bookroom.test/host/listRoomPost' ? 'active' : ''}}"><a href="{{route('list.room.post')}}" class="nav-link">Danh sách phòng đã đăng lên</a></li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <div style="margin-right: 0px;" class="dropdown">
                <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{Auth::user()->name}}
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <a class="dropdown-item" href="#">Profile</a>
                  <a class="dropdown-item" href="{{route('logout')}}">
                    Log Out
                    <svg width="16" height="16" fill="currentColor" class="bi bi-box-arrow-right"
                        viewBox="0 0 16 16">
                        <path fill-rule="evenodd"
                            d="M10 12.5a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v2a.5.5 0 0 0 1 0v-2A1.5 1.5 0 0 0 9.5 2h-8A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-2a.5.5 0 0 0-1 0v2z" />
                        <path fill-rule="evenodd"
                            d="M15.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 0 0-.708.708L14.293 7.5H5.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3z" />
                    </svg>
                  </a>
                </div>
            </div>
        </ul>
    </div>
    </div>
</nav>