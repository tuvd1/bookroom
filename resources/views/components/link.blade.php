<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
<link rel="stylesheet" href="{{asset('ui/css/open-iconic-bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('ui/css/animate.css')}}">
<link rel="stylesheet" href="{{asset('ui/css/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{asset('ui/css/owl.theme.default.min.css')}}">
<link rel="stylesheet" href="{{asset('ui/css/magnific-popup.css')}}">
<link rel="stylesheet" href="{{asset('ui/css/aos.css')}}">
<link rel="stylesheet" href="{{asset('ui/css/ionicons.min.css')}}">
<link rel="stylesheet" href="{{asset('ui/css/bootstrap-datepicker.css')}}">
<link rel="stylesheet" href="{{asset('ui/css/jquery.timepicker.css')}}">  
<link rel="stylesheet" href="{{asset('ui/css/flaticon.css')}}">
<link rel="stylesheet" href="{{asset('ui/css/icomoon.css')}}">
<link rel="stylesheet" href="{{asset('ui/css/style.css')}}">
<link rel="stylesheet" href="{{asset('css/upload.css')}}">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.3/dropzone.css" integrity="sha512-7uSoC3grlnRktCWoO4LjHMjotq8gf9XDFQerPuaph+cqR7JC9XKGdvN+UwZMC14aAaBDItdRj3DcSDs4kMWUgg==" crossorigin="anonymous" referrerpolicy="no-referrer" />