@extends('layouts.main')

@section('tittle')
    <title>Sửa thông tin bài viết</title>
@endsection

@section('link')
    @include('components.link')
@endsection

@include('components.alert_message')

@section('menu')
    @include('components.host_menu')
@endsection

@section('content')
    <section class="ftco-section contact-section goto-here">
        <div class="container">
            <div class="col-md-12 heading-section text-center ftco-animate mb-5">
                <span class="subheading">What we do</span>
              <h2 class="mb-2">Bạn có thể đăng bài tại đây</h2>
            </div>
            <div class="row block-9 justify-content-center mb-5">
                <div class="col-md-9 align-items-stretch d-flex">
                <form action="{{route('update.room.post')}}" class="bg-light p-5 contact-form" id="adddrop"  enctype="multipart/form-data" method="POST">
                    @csrf
                    @method('put')
                    @include('components.input', ['label'=>'Id', 'hidden'=>'hidden', 'type'=>'hidden', 'name'=>'id', 'placeHolder'=>'Id', 'value' => $detailRoom['id']])
                    @include('components.input', ['label'=>'Tiêu đề', 'type'=>'text', 'name'=>'tittle', 'placeHolder'=>'Tiêu đề', 'value' => $detailRoom['tittle']])
                    @include('components.input', ['label'=>'Mô tả', 'type'=>'text', 'name'=>'content', 'placeHolder'=>'Mô tả', 'value' => $detailRoom['content']])
                    @include('components.input', ['label'=>'Diện tích', 'type'=>'text', 'name'=>'area', 'placeHolder'=>'Diện tích', 'value' => $detailRoom['area']])
                    @include('components.input', ['label'=>'Giá phòng', 'type'=>'text', 'name'=>'cost', 'placeHolder'=>'Giá phòng', 'value' => $detailRoom['cost']])
                    @include('components.input_file_multi', ['label'=>'Chọn ảnh', 'type'=>'file', 'name'=>'file', 'multi'=>'multiple'])
                    <div class="form-group">
                        <div class="form-group last mb-4">
                            <span>Tỉnh:</span><br>
                            <select class="form-select province_city" name="province_id" id="select" aria-label="Default select example">
                                <option value="{{$detailRoom['village']['town']['province_cities']['code']}}">{{$detailRoom['village']['town']['province_cities']['name']}}</option>
                                @foreach ($province as $item)
                                    <option value="{{$item['code']}}">{{$item['name']}}</option>
                                @endforeach
                            </select>
                            @error('province_id')
                                <span style="color: red;">{{$message}}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <div class="form-group last mb-4">
                                <span>Huyện:</span><br>
                                <select class="form-select town" name="town_id" id="" aria-label="Default select example">
                                    <option value="{{$detailRoom['village']['town']['code']}}">{{$detailRoom['village']['town']['name']}}</option>
                                </select>
                                @error('town_id')
                                    <span style="color: red;">{{$message}}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                             <div class="form-group last mb-4">
                                <span>Xã:</span><br>
                                <select class="form-select village" name="village_id" id="" aria-label="Default select example">
                                    <option value="{{$detailRoom['village']['code']}}">{{$detailRoom['village']['name']}}</option>
                                </select>
                                @error('village_id')
                                    <span style="color: red;">{{$message}}</span>
                                @enderror
                            </div> 
                        </div>       
                    </div>
                    @include('components.input_submit', ['value'=>'Đăng bài'])
                </form>
            <div>
        </div>
    </section>

@endsection

@section('js')
    @include('components.js')
@endsection