@extends("layouts.main")

@section('tittle')
    <title>Danh sách phòng đã đăng</title>
@endsection

@section('link')
    @include('components.link')
@endsection

@include('components.alert_message')

@section('menu')
    @include('components.host_menu')
@endsection

@section('content')
<section class="ftco-section goto-here">
    <div class="container">
        <div class="col-md-12 heading-section text-center ftco-animate mb-5">
            <span class="subheading">What we do</span>
          <h2 class="mb-2">Danh sách phòng mà bạn đã đăng lên</h2>
        </div>
    <div class="row">
        <table style="text-align: center;" class="table">
            <thead>
              <tr>
                <th scope="col">Id</th>
                <th scope="col">Tiêu đề</th>
                <th scope="col">Giá</th>
                <th scope="col">Diện tích</th>
                <th scope="col">Hành động</th>
              </tr>
            </thead>
            <tbody>
                @foreach($listRoom as $item)
                <tr>
                    <th scope="col">{{$item['id']}}</th>
                    <th scope="col">{{$item['tittle']}}</th>
                    <th scope="col">{{$item['cost']}}</th>
                    <th scope="col">{{$item['area']}}</th>
                    <th scope="col">
                        <a class="btn btn-primary" href="{{route('edit.room.post', ['room_id' => $item['id']])}}">Cập nhật bài đăng</a>
                        <a class="btn btn-danger" onclick="return confirm('Are you sure?')" href="{{route('delete.room.post', ['id' => $item['id']])}}">Xóa bài đăng</a>
                    </th>
                </tr>
                @endforeach
            </tbody>
          </table>
    </div>
    </div>
</section>	
@endsection

@section('js')
    @include('components.js')
@endsection