<?php

namespace App\Http\Controllers;

use App\Models\RoomModel;
use App\Models\UserModel;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    private $user;
    private $room;

    public function __construct()
    {
        $this->user = new UserModel();
        $this->room = new RoomModel();
    }

    public function indexAdmin()
    {   
        return view('admins.index');
    }

    public function showListUser(Request $request)
    {
        $listUser = $this->user->getListUserWithLevel($request);
        return view('admins.admin_list',[
            'listUser' => $listUser,
            'level' => $request->level,
        ]);
    }

    public function showLisRoom()
    {
        $listRoom = $this->room->getAllRoom();
        return view('admins.admin_list',[
            'listRoom' => $listRoom,
            'room' => 'room',
        ]);
    }
}