<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\UserRequest;
use App\Models\ProvinceCityModel;
use App\Models\TownModel;
use App\Models\UserModel;
use App\Models\VillageModel;
use Illuminate\Http\Request;

class Authentication extends Controller
{
    private $user;
    private $town;
    private $village;
    public function __construct()
    {
        $this->user = new UserModel();
        $this->town = new TownModel();
        $this->village = new VillageModel();
    }
    public function login()
    {
        return view('login.login');
    }

    public function startLogin(LoginRequest $request)
    {
        $route = $this->user->checkLogin($request);
        return redirect()->route("$route")->with('success', 'Đăng nhập thành công');
    }

    public function sigin()
    {
        $province = ProvinceCityModel::all();
        return view('sigin.sigin', compact('province'));
    }

    public function startSigin(UserRequest $request)
    {
        $this->user->insertUser($request);
        return redirect()->route('login');
    }

    public function logOut(Request $request)
    {
        $this->user->logOut($request);
        return redirect()->route('login');
    }

    public function getVillages(Request $request)
    {
        $village= $this->village->getCodeVillage($request);
        return json_encode($village);
    }

    public function getTowns(Request $request)
    {
        $town= $this->town->getCodeTown($request);
        return json_encode($town);
    }
}