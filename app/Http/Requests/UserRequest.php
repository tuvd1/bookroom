<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'userName' => 'required|min:5|max:191|unique:mysql.users,user_name,'.request()->id,
            'pass' => 'required|min:8|max:191',
            'cfpass' => 'required|same:pass',
            'name' => 'required|max:191',
            'email' => 'required|email:rfc|unique:mysql.users,email,'.request()->id,
            'phoneNum' => 'required|max:11',
            'province_id' => 'required',
            'town_id' => 'required',
            'village_id' => 'required',
            'age' => 'required|integer|between:18,90',
            'address' => 'required',
        ];
    }
}