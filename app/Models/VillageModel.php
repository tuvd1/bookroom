<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VillageModel extends Model
{
    use HasFactory;
    protected $table = "villages";
    protected $fillable =['code', 'name', 'type', 'code_town'];

    public function town()
    {
        return $this->belongsTo(TownModel::class, 'code_town', 'code');
    }

    public function room()
    {
        return $this->hasMany(RoomModel::class, 'code_village', 'code');
    }

    //Xử lý với database
    public function getCodeVillage($request)
    {
        return VillageModel::where('code_town',$request->id)->pluck('name','code');
    }
}