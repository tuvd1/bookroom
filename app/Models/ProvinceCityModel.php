<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ProvinceCityModel extends Model
{
    use HasFactory;
    protected $table = "province_cities";
    protected $fillable =['code', 'name', 'type'];

    //Relationship
    public function town()
    {
        return $this->hasMany(TownModel::class, 'code_province_city', 'code');
    }
    public function village()
    {
        return $this->hasManyThrough(VillageModel::class, TownModel::class, 'code_province_city', 'code_town', 'code');
    }

    //Xử lý với database
    public function filter()
    {
        return ProvinceCityModel::with(['town' => function ($query)
        {
            $query->where('code', Auth::user()->town_id);
        }, 'town.room.image', 'town.room.user'])->where('code', Auth::user()->province_id)->get()->toArray();
    }
}