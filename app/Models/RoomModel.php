<?php

namespace App\Models;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class RoomModel extends Model
{
    use HasFactory;
    protected $table = "rooms";
    protected $fillable =['tittle', 'content', 'area', 'cost', 'code_village', 'user_id'];

    //Relationship
    public function image()
    {
        return $this->hasMany(ImageModel::class, 'id_room', 'id');
    }

    public function user()
    {
        return $this->belongsTo(UserModel::class, 'user_id', 'id');
    }

    public function booking()
    {
        return $this->hasMany(BookingModel::class, 'id_room', 'id');
    }

    public function village()
    {
        return $this->belongsTo(VillageModel::class, 'code_village', 'code');
    }
    public function town()
    {
        return $this->hasManyThrough(TownModel::class, VillageModel::class, 'code_town', 'code', 'code_village', 'code');
    }
    //Xử lý với database
    //Đăng một bài cho thuê phòng mới lên và thêm ảnh cho phòng đó
    public function addNewRoom($request)
    {
        if ($request->hasFile('file')) {
            $room = RoomModel::create([
                'tittle' => $request->tittle, 
                'content' => $request->content, 
                'area' => $request->area, 
                'cost' => $request->cost, 
                'code_village' => $request->village_id,
                'user_id' => Auth::id(),
            ]);
            foreach($request->file as $value){
                ImageModel::create([
                    'id_room' => $room->id, 
                    'url' => $room->id."/".$value->getClientOriginalName(), 
                    'img_type' => "test",
                ]);
                $value->move("room_images/".$room->id, $value->getClientOriginalName());
            }
            BookingModel::create([
                'id_user' => 0, 
                'id_room' => $room->id, 
                'status' => "Booking",
                'id_host' => Auth::id(), 
            ]);
            return 'list.room.post';
        }
        else{
            return 'host.index';
        }
    }

    public function updateRoom($request)
    {
        if ($request->hasFile('file')) {
            RoomModel::where('id', $request->id)->update([
                'tittle' => $request->tittle, 
                'content' => $request->content, 
                'area' => $request->area, 
                'cost' => $request->cost, 
                'code_village' => $request->village_id,
                'user_id' => Auth::id(),
            ]);
            ImageModel::where('id_room', $request->id)->delete();
            foreach($request->file as $value){
                ImageModel::create([
                    'id_room' => $request->id, 
                    'url' => $request->id."/".$value->getClientOriginalName(), 
                    'img_type' => "test",
                ]);
                $value->move("room_images/".$request->id, $value->getClientOriginalName());
            }
            return 'list.room.post';
        }
        else{
            return 'host.index';
        }
    }

    //Lọc các room theo điều kiện
    public function filter($request)
    {
        $province_code = $request->province_id;
        $town_code = $request->town_id;
        $cost = $request->cost;
        $area = $request->area;
        if(!empty($province_code) && !empty($cost) && !empty($area))
        {
            return RoomModel::with(['image', 'user'])->whereHas('village.town', function ($query) use ($town_code) {
                                                        $query->where('code',$town_code);
                                                    })->whereHas('booking', function ($query) {
                                                        $query->where('status','Booking');
                                                    })->where('cost', ">=", $cost)
                                                      ->where('cost', "<=", $cost+1)
                                                      ->where('area', ">=", $area)
                                                      ->where('area', "<=", $area+5)->get()->toArray(); 
        }
        if(empty($province_code) && !empty($cost) && !empty($area))
        {
            return RoomModel::with(['image', 'user'])->whereHas('booking', function ($query) {
                                                    $query->where('status', 'Booking');
                                                })
                                                ->where('cost', ">=", $cost)
                                                ->where('cost', "<=", $cost+1)
                                                ->where('area', ">=", $area)
                                                ->where('area', "<=", $area +5)->get()->toArray();
        }
        if(!empty($province_code) && !empty($cost) && empty($area))
        {
            return RoomModel::with(['image', 'user'])->whereHas('village.town', function ($query) use ($town_code) {
                                                        $query->where('code',$town_code);
                                                    })->whereHas('booking', function ($query) {
                                                        $query->where('status','Booking');
                                                    })->where('cost', ">=", $cost)
                                                      ->where('cost', "<=", $cost+1000000)->get()->toArray();
        }
        if(!empty($province_code) && empty($cost) && !empty($area))
        {
            return RoomModel::with(['image', 'user'])->whereHas('village.town', function ($query) use ($town_code) {
                                                        $query->where('code',$town_code);
                                                    })->whereHas('booking', function ($query) {
                                                        $query->where('status','Booking');
                                                    })->where('area', ">=", $area)
                                                      ->where('area', "<=", $area+5)->get()->toArray();  
        }
        if(!empty($province_code) && empty($cost) && empty($area))
        {
            return RoomModel::with(['image', 'user'])->whereHas('village.town', function ($query) use ($town_code) {
                                                        $query->where('code',$town_code);
                                                    })->whereHas('booking', function ($query) {
                                                        $query->where('status','Booking');
                                                    })->get()->toArray();
        }
        if(empty($province_code) && empty($cost) && !empty($area))
        {
            return RoomModel::with(['image', 'user'])->whereHas('booking', function ($query) {
                                                                    $query->where('status','Booking');
                                                                })->where('area', ">=", $area)
                                                                  ->where('area', "<=", $area+5)->get()->toArray();                                              
        }
        else{
            
        }
    }

    //Lấy thông tin phòng theo người đăng
    public function getDetailRoomByUser()
    {
        return RoomModel::where('user_id', Auth::id())->get()->toArray();
    }

    //Lấy thông tin chi tiết của phòng theo id từ database
    public function getDetailRoom($request)
    {
        return RoomModel::with('user', 'image', 'village.town.province_cities')->where('id', $request->room_id)->get()->first()->toArray();
    }   

    //Xóa thông tin phòng
    public function deleteRoom($request)
    {
        RoomModel::where('id', $request->id)
                   ->where('user_id', Auth::id())->delete();
        BookingModel::where('id_host', Auth::id())
                      ->where('id_room', $request->id)->delete();
    }

    //Lấy ra toàn bộ các phòng được đăng lên
    public function getAllRoom()
    {
        return RoomModel::all()->toArray();
    }
}